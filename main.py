import telebot
from telebot import types

TOKEN = '6930512321:AAEDuMbfKNqdD0pezjhuj35_WgPJgr7Bou4'

bot = telebot.TeleBot(TOKEN)

@bot.message_handler(commands=['start'])
def start(message):
    user_name = message.from_user.first_name
    bot.reply_to(message, f"Привет, {user_name}! Этот бот предназначен для фитнеса и похудения.")

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    training_button = types.KeyboardButton("🏋️‍♀️Тренировка🧘🏻‍♀️")
    nutrition_button = types.KeyboardButton("🥚Питание🥗")
    markup.add(training_button,nutrition_button)

    bot.send_message(message.chat.id, "Выберите раздел:", reply_markup=markup)


@bot.message_handler(func=lambda message: message.text == "🏋️‍♀️Тренировка🧘🏻‍♀️")
def training(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    fat_loss_button = types.KeyboardButton("🏃🏻Похудение/Сушка🏃🏻‍♀️")
    muscle_gain_button = types.KeyboardButton("🏋🏻Набор Мышечной Массы💪🏻")
    girl_out_button = types.KeyboardButton("🙋🏻Для Девушек🏃🏻‍♀️")
    back_button = types.KeyboardButton("Назад")
    markup.add(fat_loss_button, muscle_gain_button, back_button, girl_out_button)

    bot.send_message(message.chat.id, "Выберите тип тренировки:", reply_markup=markup)

@bot.message_handler(func=lambda message: message.text == "🏃🏻Похудение/Сушка🏃🏻‍♀️")
def fat_loss(message):
    link = "http://telegra.ph/Trenirovok-dlya-nabora-myshechnoj-massy-09-23"
    bot.send_message(message.chat.id, f"Отлично! Для похудения и сушки вот подходящие тренировки: {link}")


@bot.message_handler(func=lambda message: message.text == "🏋🏻Набор Мышечной Массы💪🏻")
def mus_loss(message):
    link = "http://telegra.ph/Trenirovka-dlya-pohudeniya-i-sushki-tela-09-23"
    bot.send_message(message.chat.id, f"Отлично! Для набор мышечной массы вот подходящее тренировки: {link}")

@bot.message_handler(func=lambda message: message.text == "🙋🏻Для Девушек🏃🏻‍♀️")
def girl(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    ass_button = types.KeyboardButton("🏃‍♀Ягодицы💃")
    exer_for_girl = types.KeyboardButton("🏃🏻‍♀️Упражнения для Девушек🙋🏼")
    add_mus = types.KeyboardButton("💪🏻Набор мышечной массы для девушек🙋🏻")
    back_button = types.KeyboardButton("назад")
    markup.add(ass_button, exer_for_girl, add_mus,back_button)
    bot.send_message(message.chat.id, "Выберите упражнение для ягодиц:", reply_markup=markup)

@bot.message_handler(func=lambda message: message.text == "🏃🏻‍♀️Упражнения для Девушек🙋🏼")
def ex_girl(message):
    link = "http://telegra.ph/Trenirovka-10-12"
    bot.send_message(message.chat.id, f"Отлично! Для девушек вот походящее тренировка: {link}")

@bot.message_handler(func=lambda message: message.text == "💪🏻Набор мышечной массы для девушек🙋🏻")
def add_mus(message):
    link = "http://telegra.ph/Nabor-myshechnoj-massy-dlya-devushek-09-27"
    bot.send_message(message.chat.id, f"Отлично! Набор мышечной массы для девушек вот походящее тренировка: {link}")



@bot.message_handler(func=lambda message: message.text == "назад")
def back(message):
    training(message)

@bot.message_handler(func=lambda message: message.text == "Назад")
def back(message):
    start(message)


@bot.message_handler(func=lambda message: message.text == "🥚Питание🥗")
def eat(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    vit = types.KeyboardButton("🥕Витамины🥝")
    sport_add = types.KeyboardButton("🛢Спортивные добавки🥛")
    markup.add(vit,sport_add)

    bot.send_message(message.chat.id, "Выберите раздел:", reply_markup=markup)


@bot.message_handler(func=lambda message: message.text == "🥕Витамины🥝")
def vitt(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    all = types.KeyboardButton("🍓Общее🍎")
    back = types.KeyboardButton("Go back")
    markup.add(all,back)
    bot.send_message(message.chat.id, "Выберите пункт меню:", reply_markup=markup)


@bot.message_handler(func=lambda message: message.text == "🍓Общее🍎")
def all_council(message):
    photo_path1 = 'img/ph1.jpg'
    bot.send_photo(message.chat.id, open(photo_path1,'rb'), caption="Общие советы по фитнесу.")


@bot.message_handler(func=lambda message: message.text == "Go back")
def back(message):
      eat(message)

@bot.message_handler(func=lambda message: message.text == "🛢Спортивные добавки🥛")
def spo_eat(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard = True)
    prot = types.KeyboardButton("🥚Протеин🥛")
    amin = types.KeyboardButton("❇️Амино⚱️")
    bcaa = types.KeyboardButton("☑️BCAA✳️")
    back = types.KeyboardButton("go back")

    markup.add(prot, amin, bcaa,back)
    bot.send_message(message.chat.id, "Выбирайте.", reply_markup=markup)

@bot.message_handler(func=lambda message: message.text == "🥚Протеин🥛")
def add_mus(message):
    link = "http://telegra.ph/Protein-09-23"
    bot.send_message(message.chat.id, f"Отлично! Набор протеина: {link}")

@bot.message_handler(func=lambda message: message.text == "❇️Амино⚱️")
def add_mus(message):
    link = "http://telegra.ph/Aminokisloty-09-23"
    bot.send_message(message.chat.id, f"Отлично! Набор витаминов амино: {link}")

@bot.message_handler(func=lambda message: message.text == "☑️BCAA✳️")
def add_mus(message):
    link = "http://telegra.ph/BCAA-09-23"
    bot.send_message(message.chat.id, f"Отлично! Набор набор витамнов bcaa : {link}")

@bot.message_handler(func=lambda message: message.text == "go back")
def back(message):
      eat(message)






bot.polling(none_stop=True)
